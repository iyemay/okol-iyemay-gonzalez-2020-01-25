import './App.scss';
import Header from "./components/Header/Header";
import ListPropertyItem from "./components/ListPropertyItem/ListPropertyItem";

function App() {
  return (
    <div className="d-flex flex-column justify-content-center mt-5 align-items-center w-100 mx-auto">
      <Header />
      <ListPropertyItem />
    </div>
  );
}

export default App;
