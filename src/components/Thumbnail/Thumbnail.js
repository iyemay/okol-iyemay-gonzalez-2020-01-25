import React from 'react';
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { createSlides } from "@splidejs/react-splide/dist/js/utils/slides";
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import imageProperties from '../../data/imageProperties.json';

export default class Thumbnails extends React.Component {

    constructor( props ) {
        super( props );

        this.primaryRef   = React.createRef();
        this.secondaryRef = React.createRef();
    }

    componentDidMount() {
        this.primaryRef.current.sync( this.secondaryRef.current.splide );
    }

    renderSlides() {
        const listSlides = imageProperties;

        return listSlides.map( slice => (
            <SplideSlide className="small-description" key={ slice.image.src }>
                <img src={ slice.image.src } alt={ slice.image.alt } />
            </SplideSlide>

        ) );
    };

    render() {
        const {property} = this.props;

        const primaryOptions = {
            type      : 'loop',
            perPage   : 1,
            perMove   : 1,
            gap       : '1rem',
            pagination: false,
        };

        const secondaryOptions = {
            type        : 'slide',
            rewind      : true,
            gap         : '1rem',
            pagination  : false,
            fixedWidth  : 55,
            fixedHeight : 35,
            cover       : true,
            focus       : 'center',
            isNavigation: true,
            updateOnMove: true,
        };

        return (
            <div className="wrapper container-relative">

                <Splide options={ primaryOptions } ref={ this.primaryRef }>
                    { this.renderSlides() }
                </Splide>

                <Splide options={ secondaryOptions } ref={ this.secondaryRef }>
                    { this.renderSlides() }
                </Splide>

                <div className="container-absolute">
                    <div className="d-flex flex-row align-items-center">
                        <i className="fa fa-circle mt-2 mt-md-0 circle-size" />
                        <p className="ml-1 mb-0 operation-text">
                            {property.type_property} en {property.type_operation}
                        </p>
                    </div>
                    <p className="mb-0 street-text">
                        {property.address.street} #{property.address.number_ext} Int. {property.address.number_int}
                    </p>
                    <p className="mb-0 state-text">{property.address.city}, {property.address.state}</p>
                    <p className="price-style font-weight-bold price-text">$ {property.price} MDP</p>
                </div>
            </div>
        );
    }
}