import React from "react";
import Thumbnails from "../Thumbnail/Thumbnail";

const ModalDetails = (props) => {
    const propertyDetails = props.property;
    return(
        <div>
            <button type="button" className="btn btn-outline-success style-button" data-toggle="modal"
                    data-target={"#modal" + propertyDetails.id}>
                Ver Propiedad
            </button>

            <div className="modal fade" id={"modal" + propertyDetails.id} tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">

                        <div className="modal-body">

                            <Thumbnails property={propertyDetails}/>

                            <div className="w-75 border shadow-sm mx-auto my-3 px-2">
                                <div className="address-container mt-2">
                                    <p className="font-weight-bold">
                                        Detalles de {propertyDetails.address.street} #{propertyDetails.address.number_ext} Int. {propertyDetails.address.number_int}
                                    </p>
                                </div>
                                <div className="mt-2">
                                    <p className="font-weight-bold mb-0">
                                        $ {propertyDetails.price} MDP
                                    </p>
                                </div>
                                <div className="d-flex flex-row align-items-center">
                                    <p className="mb-0">
                                        {propertyDetails.type_property} en {propertyDetails.type_operation}
                                    </p>
                                </div>
                                <div>
                                    <p className="description-details my-3">
                                        {propertyDetails.description}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ModalDetails;