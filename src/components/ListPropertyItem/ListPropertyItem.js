import React, { Component } from "react";
import { connect } from 'react-redux';
import PropertyItem from "../PropertyItem/PropertyItem";

class ListPropertyItem extends Component {
    render(props) {
        const propertiesToShow = this.props.listProperties;
        return(
            <div className="w-100 w-md-75">
                {propertiesToShow.map(propertyItem => {
                    return (
                        <PropertyItem key={propertyItem.property.id} property={propertyItem.property}/>
                    );
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    listProperties: state.listProperties
});

export default connect(mapStateToProps, null)(ListPropertyItem);