import React from "react";
import ModalDetails from "../ModalDetails/ModalDetails";

const PropertyItem = (props) => {

    const itemProperty = props.property;
    return(
        <div className="d-flex flex-row w-100 mx-auto shadow mt-4 mt-md-5 card-property">
            <div id="image-div" className="d-flex">
                <div className="image-container">
                    <div className="green-container">
                        <p className="new-text mb-0">Nuevo</p>
                    </div>
                </div>
            </div>
            <div className="d-flex flex-column w-75 details-container border px-3 py-3">
                <div className="d-flex flex-column flex-md-row justify-content-between">
                    <p className="mb-0 font-weight-bold">
                        {itemProperty.address.street} #{itemProperty.address.number_ext} Int. {itemProperty.address.number_int}
                    </p>
                    <p className="mb-md-0 font-weight-bold price-style">
                        $ {itemProperty.price} MDP
                    </p>
                </div>
                <div className="d-flex flex-row align-items-md-center">
                    <i className="fa fa-circle mt-2 mt-md-0 circle-size" />
                    <p className="ml-1 mb-0 text-justify">
                        {itemProperty.type_operation} de {itemProperty.type_property} en {itemProperty.address.city}, {itemProperty.address.state}
                    </p>
                </div>
                <div className="mt-4 description-text">
                    <p>
                        {itemProperty.description}
                    </p>
                </div>
                <div className="d-flex justify-content-end">
                    <ModalDetails key={itemProperty.id} property={itemProperty}/>
                </div>
            </div>
        </div>
    );
}

export default PropertyItem;