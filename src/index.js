import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import Header from "./components/Header/Header.scss";
import PropertyItem from "./components/PropertyItem/PropertyItem.scss";
import ModalDetails from "./components/ModalDetails/ModalDetails.scss";
import ListPropertyItem from "./components/ListPropertyItem/ListPropertyItem.scss";
import Thumbnails from "./components/Thumbnail/Thumbnail.scss";
import { createStore, applyMiddleware } from 'redux';
import { propertyReducer } from "./reducers/reducer";
import {logger} from "redux-logger/src";
import { Provider } from 'react-redux';
import {getProperties} from "./actions/actions";

const store = createStore(
    propertyReducer,
    applyMiddleware(logger)
);

store.dispatch(getProperties());

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);
