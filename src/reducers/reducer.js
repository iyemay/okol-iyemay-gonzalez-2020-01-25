import properties from '../data/properties.json';
import { GET_PROPERTIES } from "../actions/actions";

const initialState = {
    listProperties: []
};

export function propertyReducer(state = initialState, action) {
    console.log('reducer', state, action);

    switch(action.type) {
        case GET_PROPERTIES:
            return {
                ...state,
                listProperties: properties
            };
        default:
            return state;
    }
}

