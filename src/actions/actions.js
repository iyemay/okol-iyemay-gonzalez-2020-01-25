export const GET_PROPERTIES = "GET_PROPERTIES";


export function getProperties(listProperties) {
    return {
        type: GET_PROPERTIES,
        listProperties: listProperties
    };
}
